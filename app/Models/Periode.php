<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Periode extends Model
{
    protected $table = 'periode';
    protected $dateFormat = 'd-M-y';
    protected $guarded = ['id'];
}
