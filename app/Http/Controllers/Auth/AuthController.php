<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\User;
use Auth;
use DB;
use Validator;
use DB;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    protected $username = 'nip';
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }
    // public function postLogin(Request $request)
    // {
    //     $this->validate($request, [
    //         $this->loginUsername() => 'required',
    //         'password' => 'required',
    //         'CaptchaCode' => 'required|valid_captcha',
    //     ]);
    // }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'nip' => 'required|max:11', 
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'CaptchaCode' => 'required|valid_captcha',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'nip' => $data['nip'],
            'role_id_user' => $data['role_id_user'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'assigned' => 0,
        ]);
    }
    public function viewrole(){

        $user = DB::table('users')
            ->join('role_user', 'role_user.id_role', '=', 'users.role_id_user')
            ->select('users.*', 'role_user.role')
            ->get();
        return $user;
    }
}
