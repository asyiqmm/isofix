<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NcrAuditee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ncr_auditee', function (Blueprint $table) {
            $table->increments('id_ncr_auditee');
            $table->longText('investigasi_ktdsn');
            $table->longText('tindakan_perbaikan');
            $table->longText('lampiran')->nullable();
            $table->integer('auditee_id');
            $table->date('tgl_rencana');

            $table->foreign('auditee_id')->references('id_auditee')->on('auditee');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
