<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TglRencana extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tgl_rencana', function (Blueprint $table) {
            $table->increments('id_tgl_rencana');
            $table->date('penyelesaian');
            $table->integer('observation_id');
            $table->integer('auditee_id');

            $table->foreign('observation_id')->references('id_observation')->on('observation');
            $table->foreign('auditee_id')->references('id_auditee')->on('auditee');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
