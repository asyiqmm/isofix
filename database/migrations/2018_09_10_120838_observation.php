<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Observation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('observation', function (Blueprint $table) {
            $table->increments('id_observation');
            $table->integer('unit_kerja');
            $table->integer('auditor_id');
            $table->string('observasi');
            $table->string('saran')->nullabel();
            $table->timestamps();
            $table->foreign('auditor_id')->references('id_auditor')->on('auditor');
            $table->foreign('unit_kerja')->references('id_unit_kerja')->on('unit_kerja');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
