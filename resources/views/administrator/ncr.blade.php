<!-- Header -->
@include('header')

<div class="all-wrapper menu-side with-side-panel">
      <div class="layout-w">
		<!-- Sidebar -->
		@include('administrator.sidebar')

		<!-- Content -->
		@include('administrator.ncr_content')
		</div>
      <div class="display-type"></div>
 </div>
 
<!-- Footer -->
@include('footer')