        <div class="content-w">
          <div class="content-i">
            <div class="content-box">
              <div class="element-wrapper">
                <div class="element-box">
                    <div class="steps-w">
                        <form action="/insert" method="post">
                        {{ csrf_field() }}
                        <div class="step-contents step-content active">
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for=""> Unit Kerja :</label><select class="form-control select2" single="true" name="unit_kerja">
                                <?php
                                  $unit=$data['unit_kerja'];
                                  $klausul=$data['klausul_iso'];
                                  $auditee=$data['auditee'];
                                ?>
                                @foreach($unit as $d)
                                <option>{{$d->unit_kerja}}</option>
                                @endforeach
                              </select>
                              </div>
                              <div class="form-group">
                                <label for=""> Auditee :</label><select class="form-control select2" single="true" name="auditee">
                                @foreach($auditee as $d)
                                <option>{{$d->name}}</option>
                                @endforeach
                              </select>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <form action="#" method="get">
                                      <p>Kategori Ketidaksesuaian</p>
                                      <p><input type="radio" name="kategori" value='mayor'/> Mayor</p>
                                      <p><input type="radio" name="kategori" value='minor'/> Minor</p>
                                </form>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="">Klausul ISO 9001</label><select class="form-control select2" multiple="true" name="klausul_iso">
                                @foreach($klausul as $d)
                                <option>{{$d->indeks}}</option>
                                @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label> Uraian Ketidaksesuaian </label><textarea class="form-control" rows="3" name="uraian_ktdsn"></textarea>
                          </div>
                          <div class="form-group">
                            <label> Bukti Ketidaksesuaian </label><textarea class="form-control" rows="3" name="bukti_ktdsn"></textarea>
                          </div>
                          <div class="form-buttons-w text-right">
                            <input class="btn btn-primary step-trigger-btn" type="submit" name="submit" value='Submit' />
                          </div>
                        </div>
                        </form>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>